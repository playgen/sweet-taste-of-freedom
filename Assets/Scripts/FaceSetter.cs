﻿using UnityEngine;

public class FaceSetter : MonoBehaviour {

	void Start ()
	{
		var mesh = GetComponent<MeshFilter>().mesh;
		Vector2[] uvs = new Vector2[mesh.vertices.Length];
		// Front
		uvs[0] = new Vector2(0.0f, 0.0f);
		uvs[1] = new Vector2(0.333f, 0.0f);
		uvs[2] = new Vector2(0.0f, 0.5f);
		uvs[3] = new Vector2(0.333f, 0.5f);
		// Top
		uvs[4] = new Vector2(0.334f, 0.5f);
		uvs[5] = new Vector2(0.666f, 0.5f);
		uvs[8] = new Vector2(0.334f, 0.0f);
		uvs[9] = new Vector2(0.666f, 0.0f);
		// Back
		uvs[6] = new Vector2(1.0f, 0.0f);
		uvs[7] = new Vector2(0.667f, 0.0f);
		uvs[10] = new Vector2(1.0f, 0.5f);
		uvs[11] = new Vector2(0.667f, 0.5f);
		// Bottom
		uvs[12] = new Vector2(0.0f, 0.5f);
		uvs[13] = new Vector2(0.0f, 1);
		uvs[14] = new Vector2(0.333f, 1);
		uvs[15] = new Vector2(0.333f, 0.5f);
		// Left
		uvs[16] = new Vector2(0.334f, 0.5f);
		uvs[17] = new Vector2(0.334f, 1);
		uvs[18] = new Vector2(0.666f, 1);
		uvs[19] = new Vector2(0.666f, 0.5f);
		// Right        
		uvs[20] = new Vector2(0.667f, 0.5f);
		uvs[21] = new Vector2(0.667f, 1);
		uvs[22] = new Vector2(1.0f, 1);
		uvs[23] = new Vector2(1.0f, 0.5f);
		mesh.uv = uvs;
	}
}
