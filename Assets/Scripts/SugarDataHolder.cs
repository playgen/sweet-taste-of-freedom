﻿using System;
using System.Collections.Generic;
using System.Linq;

using PlayGen.SUGAR.Common;
using PlayGen.SUGAR.Common.Authorization;
using PlayGen.SUGAR.Contracts;
using PlayGen.SUGAR.Unity;
using PlayGen.Unity.Utilities.Loading;

using UnityEngine;

public static class SugarDataHolder
{
	public const string SugarKey = "Sugar";
	public const string SpeedKey = "SpeedPower";
	public const string SizeKey = "SizePower";
	public const string TimeKey = "TimePower";
	public const string ControlKey = "ControlPower";
	public const string AIRateKey = "AIRatePower";

	public static Dictionary<string, long> Levels = new Dictionary<string, long>
	{
		{ SpeedKey, 0 },
		{ SizeKey, 0 },
		{ TimeKey, 0 },
		{ ControlKey, 0 },
		{ AIRateKey, 0 }
	};
	public static Dictionary<string, long> GroupLevels = new Dictionary<string, long>
	{
		{ SpeedKey, 0 },
		{ SizeKey, 0 },
		{ TimeKey, 0 },
		{ ControlKey, 0 },
		{ AIRateKey, 0 }
	};
	public static Dictionary<string, long> Costs = new Dictionary<string, long>
	{
		{ SpeedKey, 10 },
		{ SizeKey, 10 },
		{ TimeKey, 30 },
		{ ControlKey, 50 },
		{ AIRateKey, 10 }
	};

	public static void Get(Action completed)
	{
		Loading.Start();
		SUGARManager.GameData.Get(success =>
		{
			Update(success.ToList());
			if (SUGARManager.CurrentGroup != null)
			{
				SUGARManager.Client.Skill.GetGameProgressAsync(SUGARManager.GameId, SUGARManager.CurrentGroup.Id,
				response =>
				{
					UpdateGroup(response.ToList());
					Loading.Stop();
					completed();
				},
				exception =>
				{
					Loading.Stop();
					completed();
				});
			}
			else
			{
				Loading.Stop();
				completed();
			}
		}, new[] { SpeedKey, SizeKey, TimeKey, ControlKey, AIRateKey });
	}

	private static void Update(List<EvaluationDataResponse> response)
	{
		long temp;
		Levels.AddOrUpdate(SpeedKey, response.Where(r => r.Key == SpeedKey && r.EvaluationDataType == EvaluationDataType.Long && long.TryParse(r.Value, out temp)).Sum(r => long.Parse(r.Value)));
		Levels.AddOrUpdate(SizeKey, response.Where(r => r.Key == SizeKey && r.EvaluationDataType == EvaluationDataType.Long && long.TryParse(r.Value, out temp)).Sum(r => long.Parse(r.Value)));
		Levels.AddOrUpdate(TimeKey, response.Where(r => r.Key == TimeKey && r.EvaluationDataType == EvaluationDataType.Long && long.TryParse(r.Value, out temp)).Sum(r => long.Parse(r.Value)));
		Levels.AddOrUpdate(ControlKey, response.Where(r => r.Key == ControlKey && r.EvaluationDataType == EvaluationDataType.Long && long.TryParse(r.Value, out temp)).Sum(r => long.Parse(r.Value)));
		Levels.AddOrUpdate(AIRateKey, response.Where(r => r.Key == AIRateKey && r.EvaluationDataType == EvaluationDataType.Long && long.TryParse(r.Value, out temp)).Sum(r => long.Parse(r.Value)));
	}

	private static void UpdateGroup(List<EvaluationProgressResponse> response)
	{
		var completedTokens = response.Where(r => Mathf.Approximately(r.Progress, 1)).Select(r => r.Token).ToList();
		foreach (var key in GroupLevels.Keys.ToList())
		{
			int level = 0;
			while (completedTokens.Contains($"{key}_{level + 1}"))
			{
				level++;
			}
			GroupLevels[key] = level;
		}
	}

	public static void Upgrade(string key, Action completed)
	{
		var upgarde = new EvaluationDataRequest
		{
			CreatingActorId = SUGARManager.CurrentUser.Id,
			EvaluationDataType = EvaluationDataType.Long,
			GameId = SUGARManager.GameId,
			Key = key,
			Value = "1"
		};
		Loading.Start();
		SUGARManager.Resource.Transfer(Platform.GlobalId, SugarKey, Costs[key] * Levels[key].Triangular(), transferred =>
		{
			if (transferred)
			{
				SUGARManager.Client.GameData.AddAsync(upgarde,
				success =>
				{
					SUGARManager.GameData.Send("Upgrade", 1);
					Get(completed);
				},
				onError =>
				{
					Get(completed);
				});
			}
			else
			{
				Get(completed);
			}
		});
	}

	public static void Transfer(string key, Action completed)
	{
		if (SUGARManager.CurrentGroup == null)
		{
			return;
		}
		var transferProgress = new EvaluationDataRequest
		{
			CreatingActorId = SUGARManager.CurrentUser.Id,
			EvaluationDataType = EvaluationDataType.Long,
			GameId = SUGARManager.GameId,
			Key = "Transfer",
			Value = "10"
		};
		Loading.Start();
		SUGARManager.Resource.Transfer(Platform.GlobalId, SugarKey, 10, transferred =>
		{
			if (transferred)
			{
				SUGARManager.Resource.Add(key + SugarKey, 10, success =>
				{
					if (success)
					{
						SUGARManager.Resource.Transfer(SUGARManager.CurrentGroup.Id, key + SugarKey, 10, groupTransfer =>
						{
							if (groupTransfer)
							{
								SUGARManager.Client.GameData.AddAsync(transferProgress,
								transferSuccess =>
								{
									Get(completed);
								},
								onError =>
								{
									Get(completed);
								});
							}
							else
							{
								Get(completed);
							}
						});
					}
					else
					{
						Get(completed);
					}
				});
			}
			else
			{
				Get(completed);
			}
		});
	}

	public static long Level(this string key)
	{
		if (Levels.ContainsKey(key) && GroupLevels.ContainsKey(key))
		{
			return Levels[key] + GroupLevels[key];
		}
		return 0;
	}

	public static void AddOrUpdate(this Dictionary<string, long> dict, string key, long value)
	{
		if (dict.ContainsKey(key))
		{
			dict[key] = value;
		}
		else
		{
			dict.Add(key, value);
		}
	}

	public static int Triangular(this long number)
	{
		var value = 0;
		for (var i = 1; i <= number + 1; i++)
		{
			value += i;
		}
		return value;
	}
}