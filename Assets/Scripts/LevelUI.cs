﻿using System;
using System.Collections;
using System.Linq;

using PlayGen.Unity.Utilities.Text;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelUI : MonoBehaviour {

	[SerializeField]
	private Image _failFade;
	[SerializeField]
	private Text _scoreText;
	[SerializeField]
	private Text _timerText;
	[SerializeField]
	private GameObject _endScreen;
	[SerializeField]
	private Text[] _endScreenNumbers;
	private bool _fading;

	private void Start()
	{
		_endScreen.SetActive(false);
	}

	public void UpdateUI(int score, float timer)
	{
		_scoreText.text = score.ToString();
		var timeSpan = new TimeSpan(0, 0, Mathf.CeilToInt(timer));
		_timerText.text = timeSpan.ToString("mm\\:ss");
	}

	public void FadeScreen()
	{
		StartCoroutine(FadeScreenCoroutine());
	}

	private IEnumerator FadeScreenCoroutine()
	{
		var endOfFrame = new WaitForEndOfFrame();
		var timer = 0f;
		while (timer <= 1.25f)
		{
			timer += Time.smoothDeltaTime;
			_failFade.color = new Color(0, 0, 0, timer);
			yield return endOfFrame;
		}
		while (timer >= 0)
		{
			timer -= Time.smoothDeltaTime;
			_failFade.color = new Color(0, 0, 0, timer);
			yield return endOfFrame;
		}
	}

	public void DisplayEndScreen(int cubes, int collected, long resource)
	{
		_timerText.text = string.Empty;
		_scoreText.text = string.Empty;
		_endScreen.SetActive(true);
		_endScreenNumbers[0].text = cubes.ToString();
		_endScreenNumbers[1].text = collected.ToString();
		_endScreenNumbers[2].text = resource.ToString();
		_endScreen.GetComponentsInChildren<Text>().Where(t => t.transform.parent != _endScreen.transform).ToList().BestFit();
	}

	public void LoadLevel()
	{
		SugarDataHolder.Get(() =>
		{
			SceneManager.LoadScene("Gameplay");
		});
	}

	public void ExitGame()
	{
		SceneManager.LoadScene("Menu");
	}
}
