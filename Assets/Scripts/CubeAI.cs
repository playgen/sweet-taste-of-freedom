﻿using System.Linq;

using UnityEngine;

public class CubeAI : MonoBehaviour
{
	private bool _moveForward;
	private bool _changingDirection;
	private bool _stopped;
	private Rigidbody _rigidbody;

	void Start ()
	{
		_rigidbody = GetComponent<Rigidbody>();
		RaycastHit[] rayHit = Physics.RaycastAll(transform.position + (Vector3.forward * 30), Vector3.down);
		_moveForward = rayHit.Any(c => c.collider.gameObject.layer == 11);
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (transform.position.y < -3)
		{
			Destroy(gameObject);
		}
		else if (!_stopped)
		{
			//_rigidbody.AddTorque((_moveForward ? Vector3.right : Vector3.back) * 2, ForceMode.VelocityChange);
			_rigidbody.angularVelocity = new Vector3(_moveForward ? _rigidbody.maxAngularVelocity : 0, 0, _moveForward ? 0 : -_rigidbody.maxAngularVelocity);
			if (!_changingDirection)
			{
				RaycastHit[] rayHit = Physics.RaycastAll(transform.position, Vector3.down);
				var platform = rayHit.FirstOrDefault(c => c.collider.gameObject.layer == 11);
				if (platform.collider != null)
				{
					if ((platform.transform.localScale.z > 1 && !_moveForward) || (platform.transform.localScale.x > 1 && _moveForward))
					{
						_changingDirection = true;
						Invoke(nameof(ChangeDirecton), (2f / Mathf.Pow(1.1f, SugarDataHolder.SizeKey.Level())) / _rigidbody.maxAngularVelocity);
					}
				}
			}
		}
	}

	private void ChangeDirecton()
	{
		_changingDirection = false;
		_moveForward = !_moveForward;
	}

	public void Stop()
	{
		_stopped = true;
	}
}
