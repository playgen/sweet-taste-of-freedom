﻿using System.Collections.Generic;
using System.Linq;

using PlayGen.SUGAR.Unity;
using PlayGen.Unity.Utilities.Loading;

using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
	[SerializeField]
	private PlayerControl _playerPrefab;
	[SerializeField]
	private GameObject _platformPrefab;
	[SerializeField]
	private GameObject _startPrefab;
	[SerializeField]
	private GameObject _finishPrefab;
	[SerializeField]
	private GameObject[] _obstaclePrefabs;

	private PlayerControl _player;
	private LevelUI _levelUI;
	private readonly List<GameObject> _platforms = new List<GameObject>();
	private readonly List<GameObject> _obstacles = new List<GameObject>();

	private float _timer = 300f;
	private float _maxTime = 300f;

	private void Start()
	{
		_levelUI = GetComponentInChildren<LevelUI>();
		CreateLevel(SugarDataHolder.ControlKey.Level() + 1);
	}

	private void Update()
	{
		if (_player)
		{
			if (_timer > 0)
			{
				_timer -= Time.smoothDeltaTime;
				if (SugarDataHolder.AIRateKey.Level() > 0 && !IsInvoking(nameof(_player.SpawnAI)) && (int)_timer % (int)(_maxTime / (SugarDataHolder.AIRateKey.Level() * 3)) == (int)(_maxTime / (SugarDataHolder.AIRateKey.Level() * 6)))
				{
					_player.SpawnAI();
					Invoke(nameof(_player.SpawnAI), 1f);
				}
			}
			else
			{
				LevelOver();
			}
		}
		if (Application.isEditor)
		{
			if (Input.GetKeyDown(KeyCode.Alpha1))
			{
				LevelOver(true);
				CreateLevel(1);
			}
			if (Input.GetKeyDown(KeyCode.Alpha2))
			{
				LevelOver(true);
				CreateLevel(2);
			}
			if (Input.GetKeyDown(KeyCode.Alpha3))
			{
				LevelOver(true);
				CreateLevel(3);
			}
			if (Input.GetKeyDown(KeyCode.Alpha4))
			{
				LevelOver(true);
				CreateLevel(4);
			}
			if (Input.GetKeyDown(KeyCode.Alpha5))
			{
				LevelOver(true);
				CreateLevel(5);
			}
		}
	}

	private void FixedUpdate()
	{
		if (_player)
		{
			_levelUI.UpdateUI(_player.SugarCollected, _timer);
		}
	}

	private void CreateLevel(long cubeAmount = 1)
	{
		DestroyLevel();
		var previousDirection = true;
		var position = Vector3.zero;
		var start = Instantiate(_startPrefab);
		start.name = "Start";
		_platforms.Add(start);
		for (var i = 0; i < 6; i++)
		{
			var randomLength = Random.Range(11, 21);
			var forwardDirection = Random.Range(0, 100) % 2 == 0;
			var platform = Instantiate(_platformPrefab);
			platform.name = _platformPrefab.name;
			platform.transform.position = position + (forwardDirection ? new Vector3(0, 0, randomLength) : new Vector3(randomLength, 0, 0));
			if (i > 0)
			{
				if (forwardDirection != previousDirection)
				{
					platform.transform.position += forwardDirection ? new Vector3(4.5f, 0, -4.5f) : new Vector3(-4.5f, 0, 4.5f);
				}
			}
			else
			{
				platform.transform.position -= forwardDirection ? new Vector3(0, 0, -10.5f) : new Vector3(-10.5f, 0, 0);
			}
			platform.transform.localScale = forwardDirection ? new Vector3(1, 1, ((randomLength * 2) + 1) * 0.1f) : new Vector3(((randomLength * 2) + 1) * 0.1f, 1, 1);
			position = platform.transform.position + (forwardDirection ? new Vector3(0, 0, randomLength + 1) : new Vector3(randomLength + 1, 0, 0));
			_platforms.Add(platform);
			if (i > 0)
			{
				var generateObstacle = Random.Range(0, (_obstaclePrefabs.Length + 1) * 50) % (_obstaclePrefabs.Length + 1);
				if (generateObstacle != _obstaclePrefabs.Length)
				{
					var obstacle = Instantiate(_obstaclePrefabs[generateObstacle]);
					obstacle.name = _obstaclePrefabs[generateObstacle].name;
					obstacle.transform.SetParent(platform.transform, true);
					if (forwardDirection)
					{
						obstacle.transform.localPosition = new Vector3(0, obstacle.transform.localPosition.y, Random.Range(-3, 4));
					}
					else
					{
						obstacle.transform.localPosition = new Vector3(Random.Range(-3, 4), obstacle.transform.localPosition.y, 0);
					}
					obstacle.transform.SetParent(null, true);
					_obstacles.Add(obstacle);
				}
			}
			previousDirection = forwardDirection;
		}
		var finish = Instantiate(_finishPrefab);
		finish.name = "Finish";
		finish.transform.position = position + (previousDirection ? new Vector3(0, 0, 9.5f) : new Vector3(9.5f, 0, 0));
		_platforms.Add(finish);
		_player = Instantiate(_playerPrefab);
		_player.name = _playerPrefab.name;
		_player.SetUpCubes(cubeAmount, finish.transform.position + (Vector3.up * 5f));
		_player.OutOfSugar += _levelUI.FadeScreen;
		_maxTime = 90f + (SugarDataHolder.TimeKey.Level() * 30f);
		_timer = _maxTime;
	}

	public void LevelOver(bool forced = false)
	{
		_player.Disable();
		_player.OutOfSugar -= _levelUI.FadeScreen;
		if (!forced)
		{
			var cubeCollected = _player.CubeCollected;
			var collected = _player.SugarCollected;
			_player = null;
			if (SUGARManager.CurrentUser != null)
			{
				Loading.Start();
				SUGARManager.GameData.Send("CubeCollected", cubeCollected);
				SUGARManager.GameData.Send("SugarCollected", collected);
				SUGARManager.GameData.Send("LevelPlayed", 1);
				SUGARManager.GameData.Send("TimePlayed", _maxTime);
				SUGARManager.Resource.Add(SugarDataHolder.SugarKey, collected, success =>
				{
					SUGARManager.Resource.Get(result =>
					{
						Loading.Stop();
						_levelUI.DisplayEndScreen(cubeCollected, collected, result.FirstOrDefault(r => r.Key == SugarDataHolder.SugarKey)?.Quantity ?? 0);
					}, new[] { SugarDataHolder.SugarKey });
				});
			}
		}
	}

	private void DestroyLevel()
	{
		foreach (var platform in _platforms)
		{
			Destroy(platform);
		}
		_platforms.Clear();
		foreach (var obstacle in _obstacles)
		{
			Destroy(obstacle);
		}
		_platforms.Clear();
		if (_player)
		{
			_player.Destroy();
		}
	}
}
