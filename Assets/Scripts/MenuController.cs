﻿using UnityEngine;

public class MenuController : MonoBehaviour {

	[SerializeField]
	private GameObject _mainMenu;
	[SerializeField]
	private GameObject _powerMenu;
	[SerializeField]
	private GameObject _sugarMenu;

	private void Start()
	{
		DisplayMainMenu();
	}

	public void DisplayMainMenu()
	{
		_mainMenu.SetActive(true);
		_powerMenu.SetActive(false);
		_sugarMenu.SetActive(false);
	}

	public void DisplayPowerMenu()
	{
		_mainMenu.SetActive(false);
		_powerMenu.SetActive(true);
		_sugarMenu.SetActive(false);
	}

	public void DisplaySugarMenu()
	{
		_mainMenu.SetActive(false);
		_powerMenu.SetActive(false);
		_sugarMenu.SetActive(true);
	}
}
