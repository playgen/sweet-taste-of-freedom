﻿using System.Linq;

using PlayGen.SUGAR.Unity;
using PlayGen.Unity.Utilities.Loading;
using PlayGen.Unity.Utilities.Text;

using UnityEngine;
using UnityEngine.UI;

public class SugarMenu : MonoBehaviour {

	[SerializeField]
	private GameObject _buttonPanel;

	private void OnEnable()
	{
		PlayGen.Unity.Utilities.Text.BestFit.ResolutionChange += BestFit;
	}

	private void OnDisable()
	{
		PlayGen.Unity.Utilities.Text.BestFit.ResolutionChange -= BestFit;
	}

	private void Update()
	{
		_buttonPanel.SetActive(!SUGARManager.Evaluation.IsActive && !SUGARManager.GameLeaderboard.IsActive && !SUGARManager.Leaderboard.IsActive && !Loading.IsActive); 
	}

	public void DisplayAchievements()
	{
		EvaluationListInterface.TitleString = "Achievements";
		SUGARManager.Evaluation.DisplayAchievementList();
	}

	public void DisplaySkills()
	{
		EvaluationListInterface.TitleString = "Skills";
		SUGARManager.Evaluation.DisplaySkillList();
	}

	public void DisplayLeaderboards()
	{
		SUGARManager.GameLeaderboard.DisplayGameList();
	}

	private void BestFit()
	{
		GetComponentsInChildren<Button>().ToList().BestFit();
	}
}
