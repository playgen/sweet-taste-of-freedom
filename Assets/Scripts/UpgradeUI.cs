﻿using System;

using PlayGen.SUGAR.Unity;

using UnityEngine;
using UnityEngine.UI;

public class UpgradeUI : MonoBehaviour {

	[HideInInspector]
	public string Key;
	public Text Name;
	public Text Level;
	public Text GroupLevel;
	public Text CostText;
	public Button Upgrade;
	public Button Group;
	public Action<string> Purchase;
	public Action<string> Transfer;

	public void SetHeader()
	{
		Name.text = "Name";
		Level.text = "Level";
		GroupLevel.text = "Group\nLevel";
		CostText.text = "Cost";
		Destroy(Upgrade.gameObject);
		Upgrade = null;
		Destroy(Group.gameObject);
		Group = null;
	}

	public void Set(string key, string powerName, long available)
	{
		Key = key;
		Name.text = powerName;
		Upgrade.onClick.AddListener(() => Purchase?.Invoke(Key));
		Group.onClick.AddListener(() => Transfer?.Invoke(Key));
		UpdateUI(available);
	}

	public void UpdateUI(long available)
	{
		if (Upgrade)
		{
			Level.text = SugarDataHolder.Levels[Key].ToString();
			GroupLevel.text = SugarDataHolder.GroupLevels[Key].ToString();
			var cost = (SugarDataHolder.Costs[Key] * SugarDataHolder.Levels[Key].Triangular());
			CostText.text = cost.ToString();
			Upgrade.interactable = cost <= available;
			Group.gameObject.SetActive(SUGARManager.CurrentGroup != null);
			Group.interactable = 10 <= available;
		}
	}
}
