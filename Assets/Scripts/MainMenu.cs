﻿using System;
using System.Linq;

using PlayGen.SUGAR.Contracts;
using PlayGen.SUGAR.Unity;
using PlayGen.Unity.Utilities.Loading;
using PlayGen.Unity.Utilities.Text;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	[SerializeField]
	private Text _userText;
	[SerializeField]
	private Text _groupText;
	[SerializeField]
	private GameObject _noGroupPrompt;
	[SerializeField]
	private GameObject _joinPrompt;
	[SerializeField]
	private InputField _joinSearch;
	[SerializeField]
	private Button[] _joinButtons;
	[SerializeField]
	private GameObject _createPrompt;
	[SerializeField]
	private InputField _createInput;
	[SerializeField]
	private GameObject _multiplePrompt;
	[SerializeField]
	private Button[] _multipleButtons;

	private void Start ()
	{
		_noGroupPrompt.SetActive(false);
		_joinPrompt.SetActive(false);
		_createPrompt.SetActive(false);
		_multiplePrompt.SetActive(false);
		if (SUGARManager.CurrentUser == null)
		{
			GetComponentInChildren<LayoutGroup>(true).gameObject.SetActive(false);
			SUGARManager.Account.DisplayPanel(success =>
			{
				if (success)
				{
					Loading.Start();
					SUGARManager.UserGroup.GetGroupsList(groups =>
					{
						Loading.Stop();
						if (groups && SUGARManager.UserGroup.Groups != null)
						{
							if (SUGARManager.UserGroup.Groups.Count == 1)
							{
								SUGARManager.CurrentGroup = SUGARManager.UserGroup.Groups.First().Actor;
								ShowMenu();
							}
							else if (SUGARManager.UserGroup.Groups.Count == 0)
							{
								_noGroupPrompt.SetActive(true);
							}
							else
							{
								_multiplePrompt.SetActive(true);
								SetMultipleGroupButtons();
							}
						}
						else
						{
							ShowMenu();
						}
					});
				}
			});
		}
		else
		{
			ShowMenu();
		}
	}

	private void OnEnable()
	{
		PlayGen.Unity.Utilities.Text.BestFit.ResolutionChange += BestFit;
	}

	private void OnDisable()
	{
		PlayGen.Unity.Utilities.Text.BestFit.ResolutionChange -= BestFit;
	}

	public void GetBiggestGroups()
	{
		Loading.Start();
		foreach (var button in _joinButtons)
		{
			button.onClick.RemoveAllListeners();
			button.gameObject.SetActive(false);
		}
		SUGARManager.Client.Group.GetAsync(success =>
		{
			var biggestGroups = success.OrderByDescending(g => g.MemberCount).Take(_joinButtons.Length).ToList();
			for (var i = 0; i < biggestGroups.Count; i++)
			{
				_joinButtons[i].gameObject.SetActive(true);
				var group = biggestGroups[i];
				_joinButtons[i].onClick.AddListener(() => JoinGroup(group));
				_joinButtons[i].GetComponentInChildren<Text>(true).text = group.Name;
			}
			_joinButtons.BestFit();
			Loading.Stop();
		},
		onError =>
		{
			Loading.Stop();
		});
	}

	public void GetSearchGroups()
	{
		Loading.Start();
		foreach (var button in _joinButtons)
		{
			button.onClick.RemoveAllListeners();
			button.gameObject.SetActive(false);
		}
		SUGARManager.Client.Group.GetAsync(_joinSearch.text, success =>
		{
			var biggestGroups = success.OrderByDescending(g => g.MemberCount).Take(_joinButtons.Length).ToList();
			for (var i = 0; i < biggestGroups.Count; i++)
			{
				_joinButtons[i].gameObject.SetActive(true);
				var group = biggestGroups[i];
				_joinButtons[i].onClick.AddListener(() => JoinGroup(group));
				_joinButtons[i].GetComponentInChildren<Text>(true).text = group.Name;
			}
			_joinButtons.BestFit();
			Loading.Stop();
		},
		onError =>
		{
			Loading.Stop();
		});
	}

	private void JoinGroup(GroupResponse group)
	{
		Loading.Start();
		var request = new RelationshipRequest
		{
			RequestorId = SUGARManager.CurrentUser.Id,
			AcceptorId = group.Id,
			AutoAccept = true
		};
		SUGARManager.Client.GroupMember.CreateMemberRequestAsync(request, success =>
		{
			SUGARManager.UserGroup.GetGroupsList(groups =>
			{
				Loading.Stop();
				if (groups && SUGARManager.UserGroup.Groups != null)
				{
					if (SUGARManager.UserGroup.Groups.Any(g => g.Actor.Id == group.Id))
					{
						SUGARManager.CurrentGroup = SUGARManager.UserGroup.Groups.First(g => g.Actor.Id == group.Id).Actor;
						_joinPrompt.SetActive(false);
						ShowMenu();
					}
				}
			});
		},
		onError =>
		{
			Loading.Stop();
		});
	}

	public void CreateGroup()
	{
		var request = new GroupRequest
		{
			Name = _createInput.text
		};
		Loading.Start();
		SUGARManager.Client.Group.CreateAsync(request, success =>
		{
			SUGARManager.UserGroup.GetGroupsList(groups =>
			{
				Loading.Stop();
				if (groups && SUGARManager.UserGroup.Groups != null)
				{
					if (SUGARManager.UserGroup.Groups.Any(g => g.Actor.Id == success.Id))
					{
						SUGARManager.CurrentGroup = SUGARManager.UserGroup.Groups.First(g => g.Actor.Id == success.Id).Actor;
						_createPrompt.SetActive(false);
						ShowMenu();
					}
				}
			});
		},
		onError =>
		{
			Loading.Stop();
		});
	}

	public void SetMultipleGroupButtons()
	{
		foreach (var button in _multipleButtons)
		{
			button.onClick.RemoveAllListeners();
			button.gameObject.SetActive(false);
		}
		var groups = SUGARManager.UserGroup.Groups.OrderBy(g => new Guid()).Take(_multipleButtons.Length).ToList();
		for (var i = 0; i < groups.Count; i++)
		{
			_multipleButtons[i].gameObject.SetActive(true);
			var group = groups[i];
			_multipleButtons[i].onClick.AddListener(delegate
			{
				SUGARManager.CurrentGroup = group.Actor;
				_multiplePrompt.SetActive(false);
				ShowMenu();
			});
			_multipleButtons[i].GetComponentInChildren<Text>(true).text = group.Actor.Name;
		}
		_multipleButtons.BestFit();
	}

	public void ShowMenu()
	{
		_userText.text = $"Welcome {SUGARManager.CurrentUser.Name}!";
		_groupText.text = SUGARManager.CurrentGroup?.Name == null ? "Not in a group" : $"Your Group: {SUGARManager.CurrentGroup.Name}";
		GetComponentInChildren<LayoutGroup>(true).gameObject.SetActive(true);
		BestFit();
	}

	public void LoadLevel()
	{
		SugarDataHolder.Get(() =>
		{
			SceneManager.LoadScene("Gameplay");
		});
	}

	public void ExitGame()
	{
		Application.Quit();
	}

	private void BestFit()
	{
		GetComponentInChildren<LayoutGroup>(true).BestFit();
	}
}
