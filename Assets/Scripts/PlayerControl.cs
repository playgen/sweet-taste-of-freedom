﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

using Random = UnityEngine.Random;

public class PlayerControl : MonoBehaviour
{
	[SerializeField]
	private Rigidbody _cubePrefab;
	[SerializeField]
	private Texture2D _botFaces;
	private List<Rigidbody> _playerCubes = new List<Rigidbody>();
	private List<Rigidbody> _aiCubes = new List<Rigidbody>();
	private List<Rigidbody> _finishedCubes = new List<Rigidbody>();

	private long _lastSpawnCount;
	private Vector3 _direction;
	private CameraControl _cameraControl;
	private Vector3 _finishPosition;
	private Bounds _finishBounds;

	public int CubeCollected => _finishedCubes.Count(c => c != null && _finishBounds.Contains(c.transform.position));
	public int SugarCollected => _finishedCubes.Where(c => c != null && _finishBounds.Contains(c.transform.position)).Sum(c => Mathf.RoundToInt(c.mass * 10));
	public Action OutOfSugar;

	private bool _canControl;
	private float _cubeSize => Mathf.Pow(1.1f, SugarDataHolder.SizeKey.Level());

	private void SetUpCubes()
	{
		SetUpCubes(_lastSpawnCount, _finishPosition);
	}

	public void SetUpCubes(long amountRoot, Vector3 finishPosition)
	{
		foreach (var cube in _playerCubes)
		{
			Destroy(cube.gameObject);
		}
		_playerCubes.Clear();
		for (var i = 0; i < amountRoot; i++)
		{
			for (var j = 0; j < amountRoot; j++)
			{
				var cube = SpawnCube(new Vector3((-(amountRoot * _cubeSize * 0.5f) + (_cubeSize * (0.5f + i))) * 2, (_cubeSize * 0.5f), (-(amountRoot * _cubeSize * 0.5f) + (_cubeSize * (0.5f + j))) * 2));
				cube.name = _cubePrefab.name;
				_playerCubes.Add(cube);
			}
		}
		_cameraControl = GetComponentInChildren<CameraControl>();
		_cameraControl.SetUpCamera(_playerCubes);
		_finishPosition = finishPosition;
		_finishBounds = new Bounds(finishPosition, new Vector3(20, 10, 20));
		_lastSpawnCount = amountRoot;
		_canControl = true;
	}

	private void Update ()
	{
		if (!_canControl)
		{
			return;
		}
		if ((_playerCubes.Count == 0 || _playerCubes.All(c => c.transform.position.y < 0)))
		{
			StartCoroutine(SpawnNewCubes());
			OutOfSugar?.Invoke();
		}
		_direction = new Vector3(Input.GetAxis("Vertical"), 0, -Input.GetAxis("Horizontal"));
		if (_direction != Vector3.zero)
		{
			MoveCubes();
		}
	}

	private void FixedUpdate()
	{
		foreach (var cube in _playerCubes)
		{
			if (_finishBounds.Contains(cube.transform.position) && !_finishedCubes.Contains(cube))
			{
				StartCoroutine(RemoveFromPlayerCubes(cube));
			}
		}

		_aiCubes = _aiCubes.Where(c => c != null).ToList();
		foreach (var cube in _aiCubes)
		{
			if (_finishBounds.Contains(cube.transform.position) && !_finishedCubes.Contains(cube))
			{
				StartCoroutine(RemoveFromAICubes(cube));
			}
		}

		_finishedCubes = _finishedCubes.Where(c => c != null).ToList();
		foreach (var cube in _finishedCubes)
		{
			if (cube.transform.position.y < -3 && !_playerCubes.Contains(cube) && !_aiCubes.Contains(cube))
			{
				Destroy(cube.gameObject);
			}
		}
	}

	private IEnumerator RemoveFromPlayerCubes(Rigidbody cube)
	{
		_finishedCubes.Add(cube);
		yield return new WaitForSeconds((10f / _cubeSize) / cube.maxAngularVelocity);
		if (!_finishBounds.Contains(cube.transform.position))
		{
			_finishedCubes.Remove(cube);
		}
		else
		{
			_playerCubes.Remove(cube);
		}
	}

	private IEnumerator RemoveFromAICubes(Rigidbody cube)
	{
		_finishedCubes.Add(cube);
		yield return new WaitForSeconds((10f / _cubeSize) / cube.maxAngularVelocity);
		if (cube != null)
		{
			if (!_finishBounds.Contains(cube.transform.position))
			{
				_finishedCubes.Remove(cube);
			}
			else
			{
				_aiCubes.Remove(cube);
				cube.GetComponent<CubeAI>().Stop();
			}
		}
	}

	private void MoveCubes()
	{
		_direction = Quaternion.Euler(0, _cameraControl.CameraRotation, 0) * _direction;
		foreach (var cube in _playerCubes)
		{
			//cube.AddTorque(_direction * 2, ForceMode.Impulse);
			cube.angularVelocity = new Vector3(_direction.x * cube.maxAngularVelocity, 0, _direction.z * cube.maxAngularVelocity);
		}
		_direction = Vector3.zero;
	}

	public void SpawnAI()
	{
		var cube = SpawnCube(new Vector3(Random.Range(-4, 5), _cubeSize * 0.5f, Random.Range(-4, 5)));
		cube.GetComponent<MeshRenderer>().material.mainTexture = _botFaces;
		cube.GetComponent<MeshRenderer>().material.color = Color.grey;
		cube.name = _cubePrefab.name + " Bot";
		cube.gameObject.AddComponent<CubeAI>();
		_aiCubes.Add(cube);
	}

	private Rigidbody SpawnCube(Vector3 cubePos)
	{
		var cube = Instantiate(_cubePrefab, cubePos, _cubePrefab.rotation);
		var checking = true;
		cube.transform.localScale = Vector3.one * _cubeSize;
		while (cube.transform.position.y < 10 && checking)
		{
			var boxCast = Physics.BoxCastAll(cube.transform.position + (Vector3.up * _cubeSize * 0.5f), cube.transform.localScale, Vector3.one, cube.rotation, 0);
			if (boxCast.Any(c => c.collider.gameObject != cube.gameObject && c.collider.gameObject.layer == 9))
			{
				cube.transform.position += (Vector3.up * 0.1f);
			}
			else
			{
				checking = false;
			}
		}
		cube.maxAngularVelocity = Mathf.Max(5, 10 + SugarDataHolder.SpeedKey.Level() - SugarDataHolder.SizeKey.Level());
		cube.mass = Mathf.Pow(_cubeSize, 3);
		cube.transform.eulerAngles = new Vector3(Random.Range(0, 3) * 90, Random.Range(0, 3) * 90, Random.Range(0, 3) * 90);
		cube.GetComponent<MeshRenderer>().material.color = Color.magenta;
		return cube;
	}

	private IEnumerator SpawnNewCubes()
	{
		_canControl = false;
		yield return new WaitForSeconds(1);
		SetUpCubes();
		_canControl = false;
		yield return new WaitForSeconds(1);
		_canControl = true;
	}

	public void Disable()
	{
		_canControl = false;
		_aiCubes = _aiCubes.Where(c => c != null).ToList();
		foreach (var cube in _aiCubes)
		{
			cube.GetComponent<CubeAI>().Stop();
		}
	}

	public void Destroy()
	{
		OutOfSugar?.Invoke();
		foreach (var cube in _playerCubes)
		{
			Destroy(cube.gameObject);
		}
		foreach (var cube in _aiCubes)
		{
			Destroy(cube.gameObject);
		}
		Destroy(gameObject);
	}
}
