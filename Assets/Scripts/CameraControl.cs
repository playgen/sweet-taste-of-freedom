﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
	private List<Rigidbody> _cubes = new List<Rigidbody>();
	private Camera _camera;
	private Vector3 _cameraOffset = new Vector3(0, 5, -7.5f);
	private Vector3 _moveVelocity = Vector3.zero;
	private float _cameraRotation;
	private float _rotateVelocity;

	public float CameraRotation => _cameraRotation;

	public void SetUpCamera(List<Rigidbody> cubes)
	{
		_cubes = cubes;
		_camera = GetComponent<Camera>();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Q))
		{
			_cameraOffset = Quaternion.Euler(0, -90, 0) * _cameraOffset;
			_cameraRotation += -90;
		}
		if (Input.GetKeyDown(KeyCode.E))
		{
			_cameraOffset = Quaternion.Euler(0, 90, 0) * _cameraOffset;
			_cameraRotation += 90;
		}
	}

	void FixedUpdate ()
	{
		if (_camera == null)
		{
			return;
		}
		var currentPosition = _camera.transform.position - _cameraOffset;
		var newPosition = _camera.transform.position - _cameraOffset;
		var iterations = 0;
		var validPoints = _cubes.Where(c => c != null && c.transform.position.y > 0).Select(c => c.transform.position).ToArray();
		while (validPoints.Length > 0 && (iterations == 0 || (Vector3.Distance(currentPosition, newPosition) > 1 && iterations < 10)))
		{
			currentPosition = newPosition;
			newPosition = new Vector3(validPoints.Sum(c => c.x) / validPoints.Length, validPoints.Sum(c => c.y) / validPoints.Length, validPoints.Sum(c => c.z) / validPoints.Length);
			validPoints = validPoints.Where(v => Vector3.Distance(newPosition, v) < 2).ToArray();
			iterations++;
		}
		_camera.transform.position = Vector3.SmoothDamp(_camera.transform.position, newPosition + _cameraOffset, ref _moveVelocity, 0.2f);
		_camera.transform.localEulerAngles = new Vector3(30, Mathf.SmoothDampAngle(_camera.transform.localEulerAngles.y, _cameraRotation, ref _rotateVelocity, 0.2f), 0);
	}
}
