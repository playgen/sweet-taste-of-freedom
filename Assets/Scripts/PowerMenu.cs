﻿using System.Collections.Generic;

using PlayGen.SUGAR.Common;
using PlayGen.SUGAR.Contracts;
using PlayGen.SUGAR.Unity;
using PlayGen.Unity.Utilities.Loading;
using PlayGen.Unity.Utilities.Text;

using UnityEngine;
using UnityEngine.UI;

public class PowerMenu : MonoBehaviour
{
	[SerializeField]
	private Text _cubeCount;
	[SerializeField]
	private UpgradeUI _upgradePrefab;
	[SerializeField]
	private Transform _upgradePanel;

	private List<UpgradeUI> _upgrades = new List<UpgradeUI>();

	private void OnEnable ()
	{
		PlayGen.Unity.Utilities.Text.BestFit.ResolutionChange += BestFit;
		Destroy();
		SugarDataHolder.Get(() =>
		{
			InvokeRepeating(nameof(UpdateUI), 0, 5);
		});
	}

	private void OnDisable()
	{
		PlayGen.Unity.Utilities.Text.BestFit.ResolutionChange -= BestFit;
		Destroy();
		CancelInvoke(nameof(UpdateUI));
	}

	private void UpdateUI()
	{
		long resourceAmount = 0;
		SUGARManager.Resource.UserGameResources.TryGetValue(SugarDataHolder.SugarKey, out resourceAmount);
		_cubeCount.text = $"Current Cube Count: {resourceAmount}";

		if (_upgrades.Count == 0)
		{
			var header = Instantiate(_upgradePrefab, _upgradePanel, false);
			header.SetHeader();
			_upgrades.Add(header);
			var speed = Instantiate(_upgradePrefab, _upgradePanel, false);
			speed.Set(SugarDataHolder.SpeedKey, "Movement Speed", resourceAmount);
			speed.Purchase += Upgrade;
			speed.Transfer += Transfer;
			_upgrades.Add(speed);
			var size = Instantiate(_upgradePrefab, _upgradePanel, false);
			size.Set(SugarDataHolder.SizeKey, "Sugar Cube Size", resourceAmount);
			size.Purchase += Upgrade;
			size.Transfer += Transfer;
			_upgrades.Add(size);
			var time = Instantiate(_upgradePrefab, _upgradePanel, false);
			time.Set(SugarDataHolder.TimeKey, "Level Time", resourceAmount);
			time.Purchase += Upgrade;
			time.Transfer += Transfer;
			_upgrades.Add(time);
			var control = Instantiate(_upgradePrefab, _upgradePanel, false);
			control.Set(SugarDataHolder.ControlKey, "Player Controlled Cubes", resourceAmount);
			control.Purchase += Upgrade;
			control.Transfer += Transfer;
			_upgrades.Add(control);
			var ai = Instantiate(_upgradePrefab, _upgradePanel, false);
			ai.Set(SugarDataHolder.AIRateKey, "AI Cube Spawn Rate", resourceAmount);
			ai.Purchase += Upgrade;
			ai.Transfer += Transfer;
			_upgrades.Add(ai);
		}
		else
		{
			foreach (var upgrade in _upgrades)
			{
				upgrade.UpdateUI(resourceAmount);
			}
		}
		BestFit();
	}

	private void Upgrade(string key)
	{
		SugarDataHolder.Upgrade(key, UpdateUI);
	}

	private void Transfer(string key)
	{
		SugarDataHolder.Transfer(key, UpdateUI);
	}

	private void Destroy()
	{
		foreach (var upgrade in _upgrades)
		{
			upgrade.Purchase -= Upgrade;
			Destroy(upgrade.gameObject);
		}
		_upgrades.Clear();
	}

	private void BestFit()
	{
		_upgradePanel.ForceOneLine().BestFit();
	}
}
